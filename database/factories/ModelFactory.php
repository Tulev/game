<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\User;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Castle::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word,
        'user_id' => App\User::all()->random()->id,
        'defence_level' => $faker->randomElement(range(1,10)),
        'x' => $faker->numberBetween(0,100),
        'y' => $faker->numberBetween(0,100),
    ];
});
