<?php

use Illuminate\Database\Seeder;

class UnitTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('unit_types')->delete();

        $unit_types = array(
            ['id' => 1, 'type' => 'Paladin', 'damage' => '60', 'life' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'type' => 'Archer',  'damage' => '100',  'life' => '50', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'type' => 'Knight',  'damage' => '100',  'life' => '200', 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        // Uncomment the below to run the seeder
        DB::table('unit_types')->insert($unit_types);
    }
}
