<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(BuildingsTableSeeder::class);
//        $this->call(ResourceTableSeeder::class);
//        $this->call(UnitTypesTableSeeder::class);
//        $this->call(ResourceUnitTypeTableSeeder::class);
//          $this->call(BuildingUnitTypeTableSeeder::class);
//        $this->call(BuildingResourceTableSeeder::class);
        $this->call(GeneralUnitTypeTableSeeder::class);
    }
}
