<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('buildings')->delete();

        $projects = array(
            ['id' => 1, 'type' => 'Goldmine', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'type' => 'Farm', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'type' => 'Barrack', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'type' => 'Stable', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        // Uncomment the below to run the seeder
        DB::table('buildings')->insert($projects);
    }
}
