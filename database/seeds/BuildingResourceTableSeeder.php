<?php

use Illuminate\Database\Seeder;

class BuildingResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('building_resource')->delete();

        $projects = array(
            ['building_id' => '1', 'resource_id' => '1',  'income' => '100', 'cost_to_build' => '20', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '1', 'resource_id' => '2',  'income' => '0', 'cost_to_build' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '1', 'resource_id' => '4',  'income' => '0', 'cost_to_build' => '10', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['building_id' => '2', 'resource_id' => '1',  'income' => '0', 'cost_to_build' => '120', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '2', 'resource_id' => '2',  'income' => '100', 'cost_to_build' => '0', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '2', 'resource_id' => '4',  'income' => '0', 'cost_to_build' => '10', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['building_id' => '3', 'resource_id' => '1',  'income' => '0', 'cost_to_build' => '150', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '3', 'resource_id' => '2',  'income' => '0', 'cost_to_build' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '3', 'resource_id' => '4',  'income' => '0', 'cost_to_build' => '20', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['building_id' => '4', 'resource_id' => '1',  'income' => '0', 'cost_to_build' => '120', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '4', 'resource_id' => '2',  'income' => '0', 'cost_to_build' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['building_id' => '4', 'resource_id' => '4',  'income' => '0', 'cost_to_build' => '30', 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        // Uncomment the below to run the seeder
        DB::table('building_resource')->insert($projects);
    }
}
