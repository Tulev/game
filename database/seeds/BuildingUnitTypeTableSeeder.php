<?php

use Illuminate\Database\Seeder;

class BuildingUnitTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('building_unit_type')->delete();

        $resource_unit_types = array(
                        // Paladin
            ['unit_type_id' => '1', 'building_id' => '1', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '1', 'building_id' => '2', 'level_required' => '0', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '1', 'building_id' => '3', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '1', 'building_id' => '4', 'level_required' => '0', 'created_at' => new DateTime, 'updated_at' => new DateTime],

                        // Archer
            ['unit_type_id' => '2', 'building_id' => '1', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '2', 'building_id' => '2', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '2', 'building_id' => '3', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '2', 'building_id' => '4', 'level_required' => '0', 'created_at' => new DateTime, 'updated_at' => new DateTime],

                        // Knight
            ['unit_type_id' => '3', 'building_id' => '1', 'level_required' => '2', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '3', 'building_id' => '2', 'level_required' => '2', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '3', 'building_id' => '3', 'level_required' => '2', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '3', 'building_id' => '4', 'level_required' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        // Uncomment the below to run the seeder
        DB::table('building_unit_type')->insert($resource_unit_types);
    }
}
