<?php

use Illuminate\Database\Seeder;

class ResourceUnitTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('resource_unit_type')->delete();

        $resource_unit_types = array(

            ['unit_type_id' => '1', 'resource_id' => '1', 'cost' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '1', 'resource_id' => '2', 'cost' => '200', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '2', 'resource_id' => '1', 'cost' => '100', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '2', 'resource_id' => '2', 'cost' => '200', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '3', 'resource_id' => '1', 'cost' => '300', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['unit_type_id' => '3', 'resource_id' => '2', 'cost' => '300', 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        // Uncomment the below to run the seeder
        DB::table('resource_unit_type')->insert($resource_unit_types);
    }
}
