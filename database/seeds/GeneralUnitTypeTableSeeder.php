<?php

use Illuminate\Database\Seeder;

class GeneralUnitTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('general_unit_type')->delete();

        $general_unit_type = array(
            ['general_id' => '11', 'unit_type_id' => '1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['general_id' => '11', 'unit_type_id' => '2', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['general_id' => '11', 'unit_type_id' => '3', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        // Uncomment the below to run the seeder
        DB::table('general_unit_type')->insert($general_unit_type);
    }
}