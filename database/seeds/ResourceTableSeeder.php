<?php

use Illuminate\Database\Seeder;

class ResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('resources')->delete();

        $projects = array(
            ['id' => 1, 'type' => 'Gold', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'type' => 'Food', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'type' => 'Wood', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'type' => 'Time', 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        // Uncomment the below to run the seeder
        DB::table('resources')->insert($projects);
    }
}
