<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingUnitTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_unit_type', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('unit_type_id')->unsigned();
            $table->integer('level_required')->unsigned();
            $table->unique(['building_id', 'unit_type_id']);
            $table->foreign('unit_type_id')->references('id')->on('unit_types');
            $table->foreign('building_id')->references('id')->on('buildings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('building_unit_type');
    }
}
