<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralUnitPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_unit', function (Blueprint $table) {
            $table->integer('general_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('amount')->unsigned();
            $table->unique(['general_id', 'unit_id']);
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('general_id')->references('id')->on('generals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_unit');
    }
}
