<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralUnitTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_unit_type', function (Blueprint $table) {
            $table->integer('general_id')->unsigned();
            $table->integer('unit_type_id')->unsigned();
            $table->integer('amount')->unsigned()->default(0);
            $table->unique(['general_id', 'unit_type_id']);
            $table->foreign('unit_type_id')->references('id')->on('unit_types');
            $table->foreign('general_id')->references('id')->on('generals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_unit_type');
    }
}
