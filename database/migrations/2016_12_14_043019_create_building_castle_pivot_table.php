<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingCastlePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_castle', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('castle_id')->unsigned();
            $table->integer('level')->unsigned()->default(0);
            $table->unique(['castle_id', 'building_id']);
            $table->foreign('castle_id')->references('id')->on('castles');
            $table->foreign('building_id')->references('id')->on('buildings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('building_castle');
    }
}
