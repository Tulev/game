<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastleResourcePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('castle_resource', function (Blueprint $table) {
            $table->integer('resource_id')->unsigned();
            $table->integer('castle_id')->unsigned();
            $table->integer('amount')->unsigned()->default(0);
            $table->unique(['resource_id', 'castle_id']);
            $table->foreign('resource_id')->references('id')->on('resources');
            $table->foreign('castle_id')->references('id')->on('castles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('castle_resource');
    }
}
