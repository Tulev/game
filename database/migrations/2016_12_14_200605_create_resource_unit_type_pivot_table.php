<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceUnitTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_unit_type', function (Blueprint $table) {
            $table->integer('resource_id')->unsigned();
            $table->integer('unit_type_id')->unsigned();
            $table->integer('cost')->unsigned();
            $table->unique(['resource_id', 'unit_type_id']);
            $table->foreign('unit_type_id')->references('id')->on('unit_types');
            $table->foreign('resource_id')->references('id')->on('resources');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resource_unit_type');
    }
}
