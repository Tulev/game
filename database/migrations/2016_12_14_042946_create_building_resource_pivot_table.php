<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingResourcePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_resource', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->integer('income')->unsigned()->default(0);
            $table->integer('cost_to_build')->unsigned();
            $table->unique(['resource_id', 'building_id']);
            $table->foreign('resource_id')->references('id')->on('resources');
            $table->foreign('building_id')->references('id')->on('buildings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('building_resource');
    }
}

