@extends('layouts.dashboard')

@section('content')

    {{--{!! Form::model($general, ['route' => ['castles.units.store', $castle->id]]) !!}--}}
    {{--{!! Form::open() !!}--}}
    {{ Form::model($general, array('route' => array('generals.battles.store', $general->id), 'method' => 'POST')) }}
        {!! Form::token() !!}
        {!! Form::label('opponents', 'The enemies!') !!}
        {!! Form::select('opponents[]', $opponents, null, ['placeholder' => 'Choose opponent!', 'class' => 'form-control ']) !!}
        {!! Form::submit('Engage!', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection