@extends('layouts.dashboard')

@section('content')


    <div class="container">
        <h3>{{$castle->name}}  Info</h3>

            {!! link_to_route('castles.units.index', 'Inspect The Army', array($castle->id), array('class' => 'btn btn-info')) !!}
        <hr>

        <div class="row">
            <div class="col-md-3">
                <h2>Resources</h2>
                   <ol class="list-group">
                       @foreach($castle->resources()->get() as $resource)
                           @if($resource->type != 'Time')
                               <li>{{$resource->type}}  : {{ $resource->pivot->amount }} </li>
                           @endif

                       @endforeach
                   </ol>
            </div>
            <div class="col-md-3">
                <h2>Buildings</h2>
                <ol class="list-group">

                    @foreach($buildings as $building)
                        <li>{{$building->type}}  on Level  : {{ $building->pivot->level }} </li>
                    @endforeach
                </ol>
            </div>
            <div class="col-md-3">
                <h2>Armies</h2>
                @foreach($generals as $general)
                    Army of "{{$general->name}}"
                        <ol class="list-group">

                            @foreach($unit_types as $type)
                                <li>{{$type->type}}   : {{ $general->countUnitsByTypeId($type->id)}} </li>
                            @endforeach
                        </ol>
                @endforeach
            </div>
        </div>

    <div class="form-group">
        <table id="js-groups" class="table table-striped" style="border-top: 1px solid #eee;">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Level</th>
                    <th>Income</th>
                    <th>TimeToUpgrade</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($buildings as $building)
                    <tr>
                        <td width="10%">{{ $building->type }}</td>
                        <td>{{ $building->pivot->level }}</td>
                        <td>Income</td>
                        <td>Time to Upgrade</td>
                        <td>
                            {!! Form::open(['url' => 'castles/'. $castle->id . '/buildings/' . $building->id , 'method' => 'PUT']) !!}
                                {!! Form::submit('Upgrade', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection