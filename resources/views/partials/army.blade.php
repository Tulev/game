@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <h3>{{$castle->name}} Armies</h3>
        <hr>
        @foreach($castle->generals as $general)

            {{$general->name}} Army
            <hr>

            {{ Form::model($general, array('route' => array('generals.battles.create', $general->id), 'method' => 'GET')) }}
            {{ csrf_field() }}
                {!! Form::submit('GO to WAR!', ['class' => 'btn btn-primary', ]) !!}
            {!! Form::close() !!}

            <hr>

                <table id="js-groups" class="table table-striped" style="border-top: 1px solid #eee;">
                    <thead>
                        <tr>
                            <th>Unit Type</th>
                            <th>Count/Numbers</th>
                            <th>Requirements</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($unit_types as $type)
                        <tr>
                            <td>{{$type->type}}</td>
                            <td>{{ $general->countUnitsByTypeId($type->id) }}</td>
                            <td>
                                @foreach($buildings as $building)
                                    {{ $building->type }} level required {{ $type->getLevelRequiredByBuildingId($building->id) }}
                                @endForeach
                            </td>
                            {{--@if($general->on_mission == true)--}}
                                <td>
                                    {!! Form::model($castle, ['route' => ['castles.units.store', $castle->id, $type]]) !!}
                                    {!! Form::hidden('type', $type->id ) !!}
                                    {!! Form::hidden('general', $general->id ) !!}
                                        {{ csrf_field() }}

                                        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            {{--@endif--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr>

        @endforeach

@endsection
