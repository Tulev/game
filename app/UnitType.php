<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitType extends Model
{
    protected $table = 'unit_types';

    protected $fillable = [
        'type',
        'damage',
        'life',
    ];

    public function units()
    {
        return $this->hasMany(Unit::class);
    }

    public function buildings()
    {
        return $this->belongsToMany(Building::class)->withPivot('level_required');
    }

    public function generals()
    {
        return $this->belongsToMany(General::class);
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class)->withPivot('cost');
    }

    public function getLevelRequiredByBuildingId($id)
    {
        if($this->buildings()->where('building_id', $id)->first() == null)
        {
            return 0;
        }

        return $this->buildings()->where('building_id', $id)->first()->pivot->level_required;
    }

    public function getResourceCost($id)
    {
        return $this->resources()->find($id)->pivot->cost;
    }

}
