<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserDashboardController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'ownerOrAdmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::findOrFail($id);
        $castles = $user->castles()->get();
        $generals = $user->generals()->get();
        //dd($generals);

        return view('layouts.dashboard', compact('user','castles', 'generals'));
    }
}
