<?php

namespace App\Http\Controllers;

use App\Battle;
use App\Castle;
use App\General;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class BattlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(General $general)
    {
        $opponents = Castle::lists('name', 'id');

        return view('battles.create', compact('opponents', 'general'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(General $general,Request $request)
    {
        $defender = $request->input('opponents');
        $defender = (int)$defender[0];
        $defending_castle = Castle::findOrFail($defender);
        $castle = $general->castle()->firstOrFail();
        dd($castle);

        $castleX = $castle->x;
        $castleY = $castle->y;

        $defending_castleX = $defending_castle->x;
        $defending_castleY = $defending_castle->y;

        $distance = pow(abs($castleX - $defending_castleX), 2) + pow(abs($castleY - $defending_castleY), 2);

        dd($general->on_mission);
        event(new BattleTakesPlace());


        $seconds_to_impact = $distance;

        $user = $general->supremeLeader()->firstOrFail();

        $user->battles()->create([
            'defending_castle_id' => $defender,
            'attacker_id' => $general->id,
            'impact_on' => Carbon::now(),
            'seconds_to_impact' => $seconds_to_impact,
        ]);



        //$attacker_firepower = ;
        //$defender_fight_power = ;



        // calc distance
        // calc seconds_to_impact
        // set attacker general field ->on mission to true



        // create new battle in DB



        // push to queue with delay time == sec_to_impact!
        // - fire event battleInAction
        // - listener to calc result
        // - if battle won -> get money


        //return "Eto mu!";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
