<?php

namespace App\Http\Controllers;

use App\Building;
use App\Castle;
use App\General;
use App\Unit;

use App\UnitType;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CastleUnitsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Castle $castle)
    {
        $unit_types = UnitType::all();
        $buildings = Building::all();
        return view('partials.army', compact('castle','unit_types', 'buildings')) ;
    }

    public function store(Castle $castle,UnitType $unit, Request $request)
    {
        $typeId = (int)$request->input('type');
        $generalId = (int)$request->input('general');
        $general = General::findOrFail($generalId);
        $unitType = UnitType::findOrFail($typeId);
        $buildings = $castle->buildings()->get();

        // check building requirements

        foreach ($buildings as $building) {
            $buildingLevel = $castle->getBuildingLevel($building->id);
            $levelRequired = $unitType->getLevelRequiredByBuildingId($building->id);

            if ($buildingLevel < $levelRequired) {
                // flash error message
                dd();
                return redirect()->back();
            }
        }

        $resources = $unitType->resources()->get();

        DB::beginTransaction();
        foreach ($resources as $resource) {



            $resourceInVault = $castle->getResourceAmountOfType($resource->type);

            $resourceRequired = $unitType->getResourceCost($resource->id);

            if ($resourceInVault < $resourceRequired) {
                DB::rollback();
                // flash error message
                return redirect()->back();
            }
            $value = $resourceInVault - $resourceRequired;

            $castle->resources()->updateExistingPivot($resource->id, ['amount' => $value]);

        }

        $currentUnitNumber = $general->countUnitsByTypeId($typeId);
        $general->unitTypes()->updateExistingPivot($typeId, ['amount' => $currentUnitNumber + 1]);
        DB::commit();

        return redirect()->back();
    }

}