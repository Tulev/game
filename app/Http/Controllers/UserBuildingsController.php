<?php

namespace App\Http\Controllers;

use App\Building;
use App\Castle;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class UserBuildingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "YO";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Building $building, Castle $castle)
    {
        // Check if castle resources are enough!

        DB::beginTransaction();

        foreach($building->resources()->where('type', '<>', 'Time' )->get() as $resource)
        {
            $castleHas = $castle->getResourceAmountOfType($resource);
            $upgradeCosts = $building->getCostOfType($resource);

            if($castleHas < $upgradeCosts)
            {
                DB::rollBack();
                return redirect()->back();
            }

            $newAmount = $castleHas - $upgradeCosts;
            $castle->resources()->updateExistingPivot($resource->id, $newAmount);

            //$model->relation()->sync([$related->id => [ 'duration' => 'someValue'] ], false);
        }

        DB::commit();;
        return redirect()->back();
       // $job = (new SendReminderEmail($user))->delay(60);


        // retrieve resources
        // pivot table level up
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
