<?php

namespace App\Http\Controllers;

use App\Building;
use App\Castle;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CastleBuildingsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Building $building
     * @param Castle $castle
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function update(Castle $castle, Building $building)
    {
        // Check if castle resources are enough!

        DB::beginTransaction();

        foreach($building->resources()->where('type', '<>', 'Time' )->get() as $resource)
        {
            $castleHas = $castle->getResourceAmountOfType($resource->type);
            $upgradeCosts = $building->getCostOfType($resource->type);

            if($castleHas < $upgradeCosts)
            {
                DB::rollBack();
                return redirect()->back();
            }

            $newAmount = $castleHas - $upgradeCosts;
            $castle->resources()->updateExistingPivot($resource->id, ['amount' => $newAmount]);

        }
        $level = $building->getLevel($castle->id) + 1;
        $castle->buildings()->updateExistingPivot($building->id, ['level' => $level]);

        //$delayTime = $building->getTimeToBuild($castle->id);


        DB::commit();

        return redirect()->back();

        // $job = (new UpgradeBuilding(Castle $castle, Building $building))->delay(60);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
