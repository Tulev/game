<?php

namespace App\Http\Controllers;

use App\Castle;
use App\General;
use App\UnitType;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class CastlesController extends Controller
{
    public function __construct()
    {
         $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        dd("castles index");
        return view('layouts.dashboard', compact('castle'));
    }

    public function show(User $user, Castle $castle)
    {
        $buildings = $castle->buildings()->get();
        $unit_types = UnitType::all();
        $generals = $castle->generals()->get();

        return view('partials.castle', compact('castle', 'buildings', 'unit_types', 'generals'));
    }

}
