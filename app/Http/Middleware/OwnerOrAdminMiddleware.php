<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OwnerOrAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && (auth()->user()->hasRole('admin') || ( auth()->user()->id == $request->user()->id) ))
        {
            return $next($request);
        }

        return redirect('/login');

    }
}
