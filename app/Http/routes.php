<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Auth::loginUsingId(8);

use App\User;

Route::get('/test', function(){

});

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::model('players', 'App\User');
Route::model('castles', 'App\Castle');
Route::model('buildings', 'App\Building');
Route::model('units', 'App\UnitType');
Route::model('generals', 'App\General');
Route::model('battles', 'App\Battle');

Route::resource('generals.battles', 'BattlesController');
Route::resource('players', 'UserDashboardController');
Route::resource('players.castles', 'CastlesController');
Route::resource('players.buildings', 'UserBuildingsController');
Route::resource('castles.buildings', 'CastleBuildingsController');
Route::resource('castles.units', 'CastleUnitsController');

Route::get('/dashboard/{user}', 'UserDashboardController@index');

Route::get('/castle/{id}', 'UserCastleController@index');



Route::group(['middleware' => ['ownerOrAdmin']], function () {
    //
});
