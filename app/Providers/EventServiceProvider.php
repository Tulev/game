<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserHasRegistered' => [
            'App\Listeners\AssignPlaceOnTheMap',
            'App\Listeners\SetInitialAmountOfResources',
            'App\Listeners\SetInitialBuildingsForEachKingdom',
            'App\Listeners\SetAGeneralForEachKingdom',
            'App\Listeners\SetSomeUnitsForEachGeneral',
        ],

        'App\Events\BattleTakesPlace' => [
            'App\Listeners\DefineWinner',
            'App\Listeners\UpdateArmies',
            'App\Listeners\SendGeneralHome',
            'App\Listeners\MakeReport',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
