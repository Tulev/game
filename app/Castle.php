<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Castle extends Model
{
    const MAX_X = 100;

    const MAX_Y = 100;

    protected $fillable = [
        'user_id',
        'name',
        'x',
        'y',
        'defence_level',
    ];

    public static function checkForCoordinates()
    {
        do {
            $x = rand(0, self::MAX_X);
            $y = rand(0, self::MAX_Y);

            $castle = Castle::where(compact('x', 'y'))->first();

        } while ($castle !== null);

        return compact('x', 'y');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class)->withPivot('amount');
    }

    public function buildings()
    {
        return $this->belongsToMany(Building::class)->withPivot('level');
    }

    public function generals()
    {
        return $this->hasMany(General::class);
    }

    public function getResourceAmountOfType($type)
    {
        $resourceType  = $this->resources()->where('type', $type)->first();
        //dd($type);
        return $resourceType->pivot->amount;
    }

    public function getGoldMineLevel()
    {
        return $this->buildings()->where('type', 'GoldMine')->first()->pivot->level;
    }

    public function getBuildingLevelByType($type)
    {
        return $this->buildings()->where('type', $type)->first()->pivot->level;
    }

    public function getBuildingLevel($id)
    {
        return $this->buildings()->find($id)->pivot->level;
    }


//    public function addResources()
//    {
//        $this->buildings()
//    }
}
