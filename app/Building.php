<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    public function castles()
    {
        return $this->belongsToMany(Castle::class)->withPivot('level');
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class)->withPivot('income', 'cost_to_build');
    }

    public function getCostOfType($type)
    {
        $resourceCost  = $this->resources()->where('type', $type)->first();

        return $resourceCost->pivot->cost_to_build;
    }

    public function getLevel($castleId)
    {
        return $this->castles()->find($castleId)->pivot->level;
    }

    public function getTimeToBuild($castleId)
    {
        $time = $this->resources()->where('type', 'Time')->first()->pivot->cost_to_build;
        $level = $this->castles()->find($castleId)->pivot->level;

        $time = $time*$level;

        return $time;
    }
}
