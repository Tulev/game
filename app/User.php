<?php

namespace App;

use App\Events\UserHasRegistered;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function boot()
    {
        parent::boot();

        static::created(function ($user){
            event(new UserHasRegistered($user));
        });
    }

    public function battles()
    {
        return $this->hasMany(Battle::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        return $this->roles($role);
    }


    public function castles()
    {
        return $this->hasMany(Castle::class);
    }

    public function generals()
    {
        return $this->hasManyThrough(General::class, Castle::class);
    }

    public function findPlaceOnTheMap()
    {

    }
}
