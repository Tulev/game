<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['amount', 'resource_type_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function castle()
    {
        return $this->belongsToMany(Castle::class);
    }

    public function unitTypes()
    {
        return $this->belongsToMany(UnitType::class)->withPivot('cost');
    }

//    public function buildings()
//    {
//        return $this->belongsToMany(Building::class);
//    }
}
