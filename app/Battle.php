<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    protected $table = 'battles';

    protected $fillable = [
        'user_id',
        'defending_castle_id',
        'attacker_id',
        'impact_on',
        'seconds_to_impact',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
