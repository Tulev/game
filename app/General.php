<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $fillable = [
        'name',
        'castle_id',
        'on_mission',
    ];

    public function supremeLeader()
    {
        return $this->belongsTo(User::class);
    }

    public function army()
    {
        return $this->hasOne(Army::class);
    }

    public function castle()
    {
        return $this->belongsTo(Castle::class);
    }

    public function unitTypes()
    {
        return $this->belongsToMany(UnitType::class)->withPivot('amount');
    }

    public function units()
    {
        return $this->belongsToMany(Unit::class)->withPivot('amount');
    }

    public function countUnitsOfTypeByTypeId($id)
    {
        return $unit = $this->units()->where('unit_type_id', $id)->count();
    }

    public function countUnitsByTypeId($id)
    {
        return $this->unitTypes()->find($id)->pivot->amount;
    }

    public function countUnitsOfType($type)
    {
        return $this->units()->where('unit_type_id', $type->id)->first()->pivot->amount;
    }

//    public function countUnitsOfType($type)
//    {
//        $unit = $this->units()->where('unit_type_id', '1')->firstOrFail();
//
//        return $unit->pivot->amount;
//    }
}
