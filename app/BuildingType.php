<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    protected $table = 'building_types';

    protected $fillable = [
        'type'
    ];

    public function buildings()
    {
        return $this->hasMany(Building::class);
    }
}
