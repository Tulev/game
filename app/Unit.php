<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';

    protected $fillable = [
        'unit_type_id',
        'level',
    ];

    public function type()
    {
        return $this->belongsTo(UnitType::class);
    }


//    // will really return only one general
    public function generals()
    {
        return $this->belongsToMany(General::class)->withPivot('amount');
    }
}
