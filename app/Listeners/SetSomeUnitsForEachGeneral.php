<?php

namespace App\Listeners;

use App\Events\UserHasRegistered;
use App\Unit;
use App\UnitType;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetSomeUnitsForEachGeneral
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event)
    {
        $generals = $event->user->generals()->get();
        foreach($generals as $general)
        {
            foreach(UnitType::all() as $type)
            {
                $unit = new Unit(['level' => '0']);

                $type->units()->save($unit);

                $general->units()->attach($unit->id, ['amount' => '100']);
            }
        }
    }
}
