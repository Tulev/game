<?php

namespace App\Listeners;

use App\Events\UserHasRegistered;
use App\Resource;
use App\ResourceType;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetInitialAmountOfResources
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event)
    {
        $castles = $event->user->castles()->get();
        $resources = Resource::all();

        foreach($castles as $castle)
        {
            foreach($resources as $resource)
            {
                $castle->resources()->attach($resource, ['amount' => 10000]);
            }
        }
    }
}
