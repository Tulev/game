<?php

namespace App\Listeners;

use App\Building;
use App\BuildingType;
use App\Events\UserHasRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetInitialBuildingsForEachKingdom
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event)
    {

        $castles = $event->user->castles()->get();
        foreach($castles as $castle)
        {

            foreach(Building::all() as $building)
            {
                $castle->buildings()->attach($building->id, ['level' => '0']);
            }
        }

    }
}
