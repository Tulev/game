<?php

namespace App\Listeners;

use App\Castle;
use App\Events\UserHasRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignPlaceOnTheMap
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event)
    {

        // what if somehow $coordinates1 = $coordinates2 ??? add recursion
        $coordinates1 = Castle::checkForCoordinates();
        $coordinates2 = Castle::checkForCoordinates();

        $event->user->castles()->saveMany([
            new Castle(['x' => $coordinates1['x'], 'y' => $coordinates1['y']]),
            new Castle(['x' => $coordinates2['x'], 'y' => $coordinates2['y']]),
        ]);

    }
}
