<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ArmyIsBackCheck::class,
        Commands\CheckForBattle::class,
        Commands\EditResources::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('game:edit-resources')->everyMinute();
//        $schedule->command('game:battle-check')->everyFiveMinutes();
//        $schedule->command('game:army-is-back')->everyFiveMinutes();

        //$schedule->command('game:send-report')->dailyAt('23:15')->sendOutputTo('path/to/file')->emailOutputTo('admin@mail.com');

    }
}
