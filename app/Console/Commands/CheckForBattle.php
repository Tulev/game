<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckForBattle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:battle-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if a battle is taking place!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
