<?php

namespace App\Console\Commands;

use App\Castle;
use App\Resource;
use App\User;
use App\Building;
use Illuminate\Console\Command;

class EditResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:edit-resources';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit resources of a player';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $castles = Castle::all();

        foreach($castles as $castle)
        {
            $resources = Resource::all();
            $buildings = Building::all();


            foreach($buildings as $building) {

                foreach ($resources as $resource) {

                    // check if !!!

                    $income =  $building->resources()->findOrFail($resource->id)->pivot->income;

                    $currentAmount = $castle->resources()->firstOrFail($resource->id)->pivot->amount;

                    $castle->resources()->editExistingPivot($resource->id, ['amount' => $currentAmount + $income]);

                }
            }
        }
    }
}
