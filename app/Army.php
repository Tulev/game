<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Army extends Model
{
    public function general()
    {
        return $this->belongsTo(General::class);
    }

//    public function castle()
//    {
//        return $this->belongsTo(Castle::class);
//    }

    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
